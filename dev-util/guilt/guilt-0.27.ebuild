# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A quilt-like system built on top of git"
HOMEPAGE="http://www.kernel.org/pub/linux/kernel/people/jsipek/guilt"
SRC_URI="mirror://kernel/linux/kernel/people/jsipek/guilt/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="doc"

DEPEND="doc? (	app-text/asciidoc
				app-text/xmlto )"
RDEPEND="dev-util/git
		 >=sys-apps/findutils-4.3"

src_compile() {
	if use doc; then
		make -C ${S}/Documentation man
	else
		einfo "Nothing to compile. :-)"
	fi
}

src_install() {
	make PREFIX=${D}/usr install
	if use doc; then
		make -C ${S}/Documentation PREFIX=${D}/usr/share install
	fi
	dodoc COPYING Documentation/HOWTO Documentation/Contributing
}
